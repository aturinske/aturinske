## Alexander Turinske's README
 
**[Govern: Security Policies](https://about.gitlab.com/handbook/engineering/development/sec/govern/security-policies/)**
 
## Related pages
 
* [GitLab](https://gitlab.com/aturinske)
* [LinkedIn](https://www.linkedin.com/in/alexanderturinske)
 
## About me
 
* I was born and raised in [**West Bend, Wisconsin, U.S.A.**](https://en.wikipedia.org/wiki/West_Bend%2C_Wisconsin).
* I value transparent and honest communication. I believe that the best ideas are a result of the discussion.
* I enjoy most outdoors things (e.g. hiking, running, rock climbing, surfing).
 
## Workstyle
 
* I believe that my success is defined as the **success of my team** and that the leader is to [**serve** the team](https://en.wikipedia.org/wiki/Servant_leadership) to achieve their goals.
* I believe that **1:1s** are the time for my team members to share concerns, and ideas and help solve blockers as most issues can be dealt with asynchronously.
* I want to be **transparent** with my **work** and **priorities**, I aim to regularly update [`#g_govern_security-policies_standup`](https://gitlab.slack.com/archives/C01UEB2GDSM) channel to share with you my progress.
* I believe in [**bias towards action**](https://about.gitlab.com/handbook/values/#bias-for-action)
* I use [**Govern: Security Policies Group Priorities**](https://about.gitlab.com/direction/govern/security_policies/#priorities) to plan next milestones.
 
## Work patterns
 
* I believe in and embrace [**non-linear workday routine**](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/):
 * I wake up around **7:00 AM** and start my workday by checking To Do's and messages to plan my day.
 * To maximize daylight hours I spend time outside often during the day, working in the morning and evening.
 
## My dashboards
 
* [Product Priorities](https://about.gitlab.com/direction/govern/security_policies/security_policy_management/#priorities)
* [My board](https://gitlab.com/groups/gitlab-org/-/boards/1754674?label_name[]=group%3A%3Asecurity%20policies&assignee_username=aturinske&milestone_title=Started)
* [Current Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1754674?milestone_title=Started&label_name[]=group%3A%3Asecurity%20policies)
* [Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1420731?label_name[]=group%3A%3Asecurity%20policies)
 
## Communicating with me
 
* I prefer [**asynchronous communication**](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication), it helps me prepare better answers and plan my workday.
* **Slack**:
  * You can send me a message on Slack at any time and I will respond during my working hours.
  * To be transparent with my team members I prefer keeping the conversation in our group channel ([#g_govern_security_policies](https://gitlab.slack.com/archives/CU9V380HW)).
  * I may send you a message on Slack outside your working hours, however, I do not expect you to respond immediately.
* **GitLab**:
  * To make sure I will see your message, please mention me (`@aturinske`) as it will automatically create a To-Do item for me.
  * When your message is related to our teamwork, consider mentioning our group handles (`@gitlab-org/govern/security-policies-backend` or `@gitlab-org/govern/security-policies-frontend`).
* **Meetings**:
  * I treat meeting attendance [optional](https://about.gitlab.com/company/culture/all-remote/meetings/#1-make-meeting-attendance-optional).
  * I am preparing the agenda for the meeting at least 24 hours before the meeting and I expect the same from attendees, this helps me collaborate before the meeting starts and better prepare for the meeting.
  * I enjoy informal coffee chats, feel free to schedule one so we can meet and understand each other better.
 
## Feedback
 
* Please **send feedback** to me in private and mark it as such. It will be most effective if you are clear and direct.
* I **appreciate all feedback** and see it as an opportunity to learn and grow.
* I aim to **provide my feedback** to you **regularly** in private during **1:1s** and **celebrate** your achievements in public, let me know if this is uncomfortable for you.
* If you want to **share feedback with me anonymously** you can use this form to do it: https://forms.gle/E2D7C4q4Qqph6uWT9. Thank you!
* Remember that [**you are not your work**](https://about.gitlab.com/handbook/values/#no-ego), I want to give you feedback to help you grow and I'd love to hear ["radical candor"](https://www.radicalcandor.com/radical-candor-not-brutal-honesty/) from you as well:
 

